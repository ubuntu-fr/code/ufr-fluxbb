FROM debian:buster-slim

RUN apt-get update
RUN apt-get install -y \
	libapache2-mod-php \
	php-mysql php-curl php-gd php-intl php-json php-mbstring php-xml php-zip \
	msmtp msmtp-mta

RUN a2enmod rewrite

WORKDIR /app

EXPOSE 80
ENTRYPOINT ["apache2ctl", "-D", "FOREGROUND"]
