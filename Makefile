color_method = \033[93m
containers = fluxbb_db fluxbb
app_name = ufr-fluxbb

help: ## Affiche ce message d'aide
	@printf "\n$(color_method)help\033[0m\n\n"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST) | sort
	@printf "\n"

start: ## Démarre l'application
	@if [ -z "$(shell docker network ls -q -f name=web)" ]; then \
		echo "web network does not exist, creating..."; \
		docker network create web; \
	fi;
	@if [ -z "$(shell docker ps -qa -f name=traefik)" ]; then \
		echo "traefik does not exist, creating..."; \
		docker-compose up -d traefik; \
	elif [ -z "$(shell docker ps -q -f name=traefik)" ]; then \
		echo "traefik is stopped, starting..."; \
		docker start traefik; \
	else \
		echo "traefik is already running, skipping."; \
	fi;
	@if [ -z "$(shell docker ps -qa -f name=mailhog)" ]; then \
		echo "mailhog does not exist, creating..."; \
		docker-compose up -d mailhog; \
	elif 	[ -z "$(shell docker ps -q -f name=mailhog)" ]; then \
		echo "mailhog is stopped, starting..."; \
		docker start mailhog; \
	else \
		echo "mailhog is already running, skipping."; \
	fi;
	@echo "starting ${app_name} containers..."
	@docker-compose up -d $(containers)
	@echo "${app_name} started, made available at http://${app_name}.localhost/"

stop: ## Arrête l'application
	docker-compose stop

logs: ## Affiche le journal en temps réel
	docker-compose logs -f

update: ## Met à jour les containers locaux
	docker-compose pull

pull: ## Récupère les modifications poussées en production
	git pull

migrate: ## Peuple la base de données
	@echo "downloading migrations"
	@wget -P migrations/ https://ubuntu-fr.crachecode.net/forum/db/ufr_fluxbb_anonym.sql
	@echo "populate database"
	@docker-compose exec -T fluxbb_db sh -c 'exec mysql -uroot -p"$$MYSQL_ROOT_PASSWORD"' < ./migrations/ufr_fluxbb_anonym.sql
	@echo "done"

clean: stop ## Supprime les containers de l'application
	docker container rm -f $(containers)
