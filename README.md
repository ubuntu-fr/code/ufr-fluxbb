# Environnement de développement FluxBB pour [forum.ubuntu-fr.org](https://forum.ubuntu-fr.org/)

## Déploiement

### Prérequis

Cet environnement dépend de [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms), qui inclut des assets généraux (images, CSS, JavaScript) nécessaires à la bonne présentation du forum.  
Déployez [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms) avant de déployer ufr-fluxbb.

### Installer Docker et Docker Compose

- [Docker](https://docs.docker.com/install/#supported-platforms)
- [Docker Compose](https://docs.docker.com/compose/install/#install-as-a-container)

(sur Ubuntu il suffit d'installer les paquets [docker.io](apt://docker.io) et [docker-compose](apt://docker-compose))

### Cloner le dépôt de sources

```sh
git clone git@gitlab.com:ubuntu-fr/code/ufr-fluxbb.git
```

### Démarrer l'application

```sh
cd ufr-fluxbb
sudo make start
```

### C'est prêt

- http://ufr-fluxbb.localhost/
- liste des commandes disponibles : `make help`


- Les CSS à personnaliser se trouvent dans le fichier `app/style/Ubuntu.css`.
- Les fichiers HTML à personnaliser se trouvent dans le répertoire `app/include/template`.
