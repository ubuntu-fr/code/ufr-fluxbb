<!DOCTYPE html>
<html lang="fr">
<head>

	<pun_include "head.php">
	<pun_head>

</head>

<body>

	<pun_include "header.php">

	<main>

		<div id="punadmin" class="pun">
		<div class="top-box"></div>
		<div class="punwrap">

		<div id="brdheader" class="block">
			<div class="box">
				<div id="brdtitle" class="inbox">
					<pun_title>
					<pun_desc>
				</div>
				<pun_navlinks>
				<pun_status>
			</div>
		</div>

		<pun_announcement>

		<div id="brdmain">
		<pun_main>
		</div>

		<pun_footer>

		</div>
		<div class="end-box"></div>
		</div>
		
	</main>

</body>
</html>
