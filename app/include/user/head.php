<?php
  $domain = $_SERVER['HTTP_HOST'];
  if (stripos($domain,'localhost') !== false) { // développement
    $main = 'http://ufr-cms.localhost';
  }
  elseif (stripos($domain,'crachecode') !== false) { // preview
    $main = 'https://ubuntu-fr.gitlab.io/code/ufr-main-layout';
  }
  else { // production
    $main = 'https://www.ubuntu-fr.org';
  }
?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="<?php echo $main ?>/favicon.png">
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo $main ?>/assets-demo/Focal-Fossa_WP_1920x1080.png">
<meta name="theme-color" content="#5E2750">
<link rel="stylesheet" href="<?php echo $main ?>/css/main.css">
<link rel="stylesheet" href="<?php echo $main ?>/css/print.css" media="print">
<script type="module" src="<?php echo $main ?>/js/app.js"></script>
