<header>
  <div class="topbar">
    <a href="https://ubuntu-fr.gitlab.io/code/ufr-cms/" id="logo">
      <img src="https://ubuntu-fr.gitlab.io/code/ufr-main-layout/img/logo.svg" alt="ubuntu-fr">
    </a>
    <div id="sandwich">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <div class="navbar">
    <nav>
      <?php
        $domain = $_SERVER['HTTP_HOST'];
        if (stripos($domain,'localhost') !== false) { // développement
      ?>
        <a href="http://ufr-cms.localhost/">Accueil</a>
        <a href="http://ufr-doc.localhost/">Documentation</a>
        <a href="http://ufr-fluxbb.localhost/" class="active">Forum</a>
        <a href="http://ufr-cms.localhost/about/">À propos</a>
      <?php
        }
        elseif (stripos($domain,'crachecode') !== false) { // preview
      ?>
        <a href="https://ubuntu-fr.gitlab.io/code/ufr-cms/">Accueil</a>
        <a href="https://ufr-doc.crachecode.net/">Documentation</a>
        <a href="https://ufr-fluxbb.crachecode.net/" class="active">Forum</a>
        <a href="https://ubuntu-fr.gitlab.io/code/ufr-cms/about">À propos</a>
      <?php
        }
        else { // production
      ?>
        <a href="https://www.ubuntu-fr.org/">Accueil</a>
        <a href="https://doc.ubuntu-fr.org/">Documentation</a>
        <a href="https://forum.ubuntu-fr.org/" class="active">Forum</a>
        <a href="https://www.ubuntu-fr.org/about/">À propos</a>
      <?php
        }
      ?>
    </nav>
    <div class="switchers">
      <div class="switcher" id="switcher-wide"><div></div><div></div></div>
      <div href="#" class="switcher" id="switcher-light"></div>
      <div href="#" class="switcher" id="switcher-dark"></div>
    </div>
  </div>
</header>