##
##
##        Mod title:  Forum announcements
##
##      Mod version:  1.1.1
##										
##  Works on FluxBB:  1.5.6
##
##     Release date:  02/05/2014
##
##           Author:  Christophe Sauthier ( sauthier.christophe@gmail.com )
##     Contributors:  Jarom Ort ( jdiff55@gmail.com )
##
##      Description:  This mod manage/display a common header announce for all the post of a forum. Announces are stored in html.
##
##          Changes:  This is the initial port to FluxBB.
##                    Lots of spelling fixes in the code. I'm pretty sure I saw at least 30 different spellings of "annonces".
##                    Fixed a bug where announcements were not visible after they were added.
##                    Changed all <?s into <?phps because nobody likes <?s.
##										
##
##   Affected files: lang/French/announcements.php (added)
##                   lang/English/announcements.php (added)
##                   plugins/AP_Forum_announcements.php (added)
##                   viewforum.php (updated)
##
##       Affects DB: yes
## 
##
##       DISCLAIMER: Please note that "mods" are not officially supported by
##                   FluxBB. Installation of this modification is done at your
##                   own risk. Backup your forum database and any and all
##                   applicable files before proceeding.
##
##



#
#---------[ 1. UPLOAD FILES ]-------------------------------------
#

/upload/install_mod.php to /
/upload/lang/English/announcements.php to /lang/English/
/upload/lang/French/announcements.php to /lang/French/
/upload/plugins/AP_Forum_Announcements.php to /plugins/

#
#---------[ 4. OPEN ]-------------------------------------------------------
#

viewforum.php

#
#---------[ 5. FIND ]-----------------------------------------------------
#

// Load the viewforum.php language file
require PUN_ROOT.'lang/'.$pun_user['language'].'/forum.php';

#
#---------[ 6. ADD AFTER ]-----------------------------------------
#

// Begin Forum announcements
require PUN_ROOT.'lang/'.$pun_user['language'].'/announcements.php';
// End Forum announcements

#
#---------[ 7. FIND ]-----------------------------------------------------
#

<div class="linkst">
	<div class="inbox crumbsplus">
		<ul class="crumbs">
			<li><a href="index.php"><?php echo $lang_common['Index'] ?></a></li>
			<li><span>»&#160;</span><strong><a href="viewforum.php?id=<?php echo $id ?>"><?php echo pun_htmlspecialchars($cur_forum['forum_name']) ?></a></strong></li>
		</ul>
		<div class="pagepost">
			<p class="pagelink conl"><?php echo $paging_links ?></p>
<?php echo $post_link ?>
		</div>
		<div class="clearer"></div>
	</div>
</div>

#
#---------[ 8. ADD AFTER ]-----------------------------------------
#

<!-- Begin Forum announcements -->
<?php
$sql = 'SELECT forum_announcement, forum_announcement_visible FROM '.$db->prefix.'announcement WHERE forum_id='.$id;

$result = $db->query($sql) or error('Unable to fetch forum announcements', __FILE__, __LINE__, $db->error());
 
// If there are topics in this forum.
if (($cur_forum_announcement = $db->fetch_assoc($result))&&($cur_forum_announcement['forum_announcement_visible']))
{
?>
<div id="announce" class="block">
<h2><span><?php echo $lang_announcements['announcement title']; ?></span></h2>
<div class="box">
<div class="inbox">
<?php

echo $cur_forum_announcement['forum_announcement'];

?>
<p>
</div>
</div>
</div>
<?php
}
?>
<!-- End Forum announcements -->

#
#---------[ 9. SAVE/UPLOAD FILE ]----------------------------
#

viewforum.php

#
#---------[ 10. DELETE ]----------------------------
#

install_mod.php

#
#---------[ 11. END & SETTINGS ]----------------------------------------------
#

To end installation, you have to go in the plugin administration section,
default config data will be inserted.




