<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<pun_language>" lang="<pun_language>" dir="<pun_content_direction>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<pun_head>
</head>

<body>



<div class="block_one">

<div id="punadmin" class="pun">
	<pun_status>
<div class="top-box"></div>
<div class="punwrap">

<div id="brdheader" class="block">
	<div class="box">
		<div id="brdtitle" class="inbox">
		
			<pun_title>
			<pun_desc>
		</div>

	</div>
</div>
</div>


<!--NAVBAR BLOCK-->
<div class="block_one">
<nav class='cf'>
  <ul class='cf'>
		<pun_navlinks>
  </ul>
  <a href='#' id='openup'><i class="fa fa-bars" aria-hidden="true" style="text-align:center;color:#fff;"></i></a>
</nav>
</div>
<!--NAVBAR BLOCK-->



<!--MAIN-->
<div class="block_two">
<div class="block_content">
<pun_announcement>
<div id="brdmain">
<pun_main>
</div>
</div>
</div>
<!--MAIN-->
<!--FOOTER-->
<div class="block_one">
<pun_footer>
</div>
<div class="end-box"></div>
</div>
</div>
<!--FOOTER-->

<script type='text/javascript' src='http://code.jquery.com/jquery-1.10.2.min.js'></script>
<script src="/style/Midnight/nav.js"></script>
</body>
</html>
